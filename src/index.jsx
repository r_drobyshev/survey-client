import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store from './store';
import RouteMap from './routes';
import './index.scss';
import './adapter';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();

const Root = (
  <Provider store={store}>
    <BrowserRouter>
      <RouteMap />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(Root, document.getElementById('root'));

