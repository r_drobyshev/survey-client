import React from 'react';
import { Redirect, Route, Switch } from 'react-router';

import AdminPage from './pages/admin/container';
import EditorPage from './pages/editor/container';
import SurveyPage from './pages/survey/container';
import ResultsPage from './pages/results/container';
import LoginPage from './pages/login/container';

const routes = [
  {
    path: '/',
    render: (props) => {
      const { location } = props;
      location.state = '/';
      return <AdminPage {...props} />;
    },
    exact: true,
  },
  {
    path: '/edit/:id',
    render: (props) => {
      const { location } = props;
      location.state = '/edit';
      return <EditorPage {...props} />;
    },
    exact: true,
  },
  {
    path: '/survey/:id',
    render: (props) => {
      const { location } = props;
      location.state = '/survey';
      return <SurveyPage {...props} />;
    },
    exact: true,
  },
  {
    path: '/results/:id',
    render: (props) => {
      const { location } = props;
      location.state = '/results';
      return <ResultsPage {...props} />;
    },
    exact: true,
  },
  {
    path: '/login',
    render: (props) => {
      const { location } = props;
      location.state = '/login';
      return <LoginPage {...props} />;
    },
    exact: true,
  },
];


const RouteMap = () => (
  <React.Fragment>
    <Switch>
      { routes.map(item => <Route {...item} key={item.path} />) }
      <Redirect exact from="*" to="/" />
    </Switch>
  </React.Fragment>
);

export default RouteMap;
