import { combineReducers } from 'redux';

import editor from '../pages/editor/reducer';
import admin from '../pages/admin/reducer';
import survey from '../pages/survey/reducer';
import results from '../pages/results/reducer';
import login from '../pages/login/reducer';

const reducers = {
  editor,
  admin,
  survey,
  results,
  login,
};

const state = combineReducers(reducers);
export default state;
