import { fork, all } from 'redux-saga/effects';

import editor from '../pages/editor/effects';
import admin from '../pages/admin/effects';
import survey from '../pages/survey/effects';
import results from '../pages/results/effects';
import login from '../pages/login/effects';

export default function* () {
  yield all([
    fork(editor),
    fork(admin),
    fork(survey),
    fork(results),
    fork(login),
  ]);
}
