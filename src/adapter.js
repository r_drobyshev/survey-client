import axios from 'axios';
import { deleteCookie } from './helper/utils';
import store from './store';
import { setRedirectToLogin } from './pages/login/actions';

axios.defaults.baseURL = process.env.REACT_APP_API;

axios.interceptors.response.use(response => response, (error) => {
  const { response: { status } } = error;
  if (status === 401) {
    deleteCookie("token");
    store.dispatch(setRedirectToLogin());
  }
  return Promise.reject(error);
});
