import React from 'react';

export const compareBlocks = (a, b) => {
  if (+a < +b) {
    return -1;
  }
  if (+a >= +b) {
    return 1;
  }
  return 0;
};

export const getCookie = (name) => {
  const matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
};

export function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

export function deleteCookie(name) {
  setCookie(name, "", {
    expires: -1
  })
}

export function getBlockDescription(id) {
  switch(id) {
    case 1: return (
      <>
        Общая информация
      </>
    );
    case 2: return (
      <>
        Инструкция: Вопросы данного блока касаются информации относительно Вашего
        поступления в вуз. Отметьте один или несколько вариантов ответа.
      </>
    );
    case 3: return (
      <>
        Инструкция: Внимательно прочитайте каждый вопрос и из предложенных к нему
        вариантов ответов выберите один – тот, который лучше всего выражает Вашу
        точку зрения. Работайте внимательно и не спешите. Здесь нет правильных и
        неправильных ответов. Ваше мнение ценно для нас таким, какое оно есть.
      </>
    );
    case 4: return (
      <>
        Инструкция: Ниже перечислены мотивы, которыми может руководствоваться
        человек при поступлении в вуз. Оцените, пожалуйста, степень соответствия
        указанных мотивов тем, которыми Вы руководствовались при поступлении в
        Институт компьютерных технологий и информационной безопасности. Оценка
        дается по пятибалльной шкале: 1- «не значимый», 2 - «малозначимый», 3 -
        «заметный», 4 - «важный», 5 - «очень важный».
      </>
    );
    case 5.1: return (
      <>
        Инструкция: Задумайтесь, каковы Ваши ожидания, и какие возможности Вы бы
        хотели реализовать в процессе обучения вузе? Оцените, пожалуйста,
        представленные ниже возможности по пятибалльной шкале: 1 – «совсем не важно»,
        2 – «скорее, не важно», 3 – «нейтрально», 4 – «важно», 5 – «очень важно».
      </>
    );
    case 5.2: return (
      <>
        Инструкция: Задумайтесь, какие из возможностей обучения в вузе уже
        реализовались? Оцените, пожалуйста, представленные возможности по
        пятибалльной шкале: 1 – «совсем не реализовано», 2 – «скорее, не реализовано», 3 –
        «затрудняюсь оценить», 4 – «частично реализовано», 5 – «полностью реализовано».
      </>
    );
    case 6: return (
      <>
        Инструкция: При ответе на следующие вопросы отметьте один или несколько (не
        более трех) вариантов ответа
      </>
    );
    case 7: return (
      <>
        Инструкция: При ответе на следующие вопросы отметьте один вариант ответа.
      </>
    );
    case 8.1: return (
      <>
        Инструкция: Оцените по пятибалльной шкале, насколько для Вас значимы
        различные аспекты обучения в вузе: 1 – «совсем не важно», 2 – «скорее, не важно», 3
        – «нейтрально», 4 – «важно», 5 – «очень важно».
      </>
    );
    case 8.2: return (
      <>
        Инструкция: Оцените по пятибалльной шкале, насколько вы удовлетворены
        различными аспектами обучения в вузе на текущий момент: 1 – «совсем не
        удовлетворен», 2 – «скорее, не удовлетворен», 3 – «нейтрально», 4 – «скорее,
        удовлетворен», 5 – «полностью удовлетворен».
      </>
    );
    case 9: return (
      <>
        Инструкция: Прочтите нижеперечисленные утверждения, касающиеся трудностей,
        с которыми встречаются студенты в процессе учебы в вузе. Оцените, насколько Вам
        характерна каждая из трудностей, насколько часто она встречается в Вашей
        жизни по следующей шкале: 2 – часто; 1 – редко; 0 – никогда.
      </>
    );
    case 10: return (
      <>
        Инструкция: прочтите вопрос, и выберете один вариант ответа.
      </>
    );
    default: return null;
  }
}

export function isValid(questions, answers, blockId) {
  const questionsByBlock = questions.filter(question => question.block_id === blockId.toString());
  const results = [];
  Object.entries(answers).forEach(([index, answer]) => {
    questionsByBlock.forEach(question => {
      if (question.type === 'multi') {
        if (question.question_id === index && answer.some(item => item)) results.push(true)
      } else {
        if (question.question_id === index && answer) results.push(true)
      }
    })
  });
  return results.length === questionsByBlock.length;
}

export function s2ab(s) {
  const buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
  const view = new Uint8Array(buf);  //create uint8array as viewer
  for (let i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
  return buf;
}
