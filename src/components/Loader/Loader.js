import React from 'react';

import './Loader.scss';

const Loader = () => (
  <div className="ms-preload show">
    <div className="spinner">
      <div className="dot1" />
      <div className="dot2" />
    </div>
  </div>
);

export default Loader;
