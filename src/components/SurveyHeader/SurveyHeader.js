import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

import './SurveyHeader.scss';

class SurveyHeader extends React.PureComponent {
  render() {
    const { props } = this;
    return (
      <div className="survey-header">
        <AppBar className="header-appbar" position="static">
          <Toolbar className="header-toolbar">
            <Typography className="header-title" variant="h6" color="inherit">
              {props.title}
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default SurveyHeader;
