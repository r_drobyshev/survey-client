import React from 'react';
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import './AdminHeader.scss';

export default class AdminHeader extends React.PureComponent {
  render() {
    return (
      <div className="header">
        <AppBar className="header-appbar" position="static">
          <Toolbar className="header-toolbar">
            <Typography variant="h6">
              <Link className="header-title" to="/">Административная панель</Link>
              <Link className="header-title mobile" to="/"><i className="material-icons">home</i></Link>
            </Typography>
            <div className="header__right-elements">
              <Button className="header__faq-button" variant="outlined">Справка</Button>
              <IconButton color="inherit" aria-label="Profile">
                <AccountCircle />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}