import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import './admin.scss';
import AdminPageMain from './AdminPageMain';
import { getSurveys } from './actions';

import AdminHeader from '../../components/AdminHeader/AdminHeader'

class AdminPage extends React.PureComponent {
  componentDidMount() {
    const { props } = this;
    props.getSurveys();
  };

  render() {
    const { props } = this;
    if (!props.isLogged) return <Redirect to="/login" />;
    if (!props.surveys) return null;
    return (
      <>
        <AdminHeader />
        <AdminPageMain />
      </>
    )
  }
}

const mapStateToProps = state => ({
  surveys: state.admin.surveys,
  isLogged: state.login.isLogged,
});

const mapDispatchToProps = {
  getSurveys,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);