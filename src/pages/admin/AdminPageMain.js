import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import SurveyCard from './components/SurveyCard/SurveyCard'

import './admin.scss';

class AdminPageMain extends React.PureComponent {
  render() {
    const { props } = this;
    const surveys = props.surveys.filter(survey => survey.survey_id !== "0");
    return (
      <div className="body-wrapper admin-main">
        <div className="survey-toolbar">
          <Link to="/edit/0">
            <Button className="add-survey-button" variant="contained">Создать</Button>
          </Link>
        </div>
        {
          props.surveys.length > 1 ? <div className="surveys-wrapper">
            <h2>Активные</h2>
            <div className="active-surveys">
              {
                surveys.filter(survey => survey.state === true).map((survey) => (
                  <SurveyCard key={survey.survey_id} survey={survey} />
                ))
              }
            </div>
            <h2>Отключенные</h2>
            <div className="disabled-surveys">
              {
                surveys.filter(survey => survey.state === false).map((survey) => (
                  <SurveyCard key={survey.survey_id} survey={survey} />
                ))
              }
            </div>
          </div> : <div className="surveys-wrapper">
            <Card>
              <CardContent>
                Пожалуйста, создайте хотя бы один опрос!
              </CardContent>
            </Card>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  surveys: state.admin.surveys,
});

export default connect(mapStateToProps, null)(AdminPageMain);