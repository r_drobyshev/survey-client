import immutable from 'seamless-immutable';

export const actionTypes = {
  GET_SURVEYS_REQUESTED: 'admin/GET_SURVEYS_REQUESTED',
  GET_SURVEYS_SUCCEEDED: 'admin/GET_SURVEYS_SUCCEEDED',
  GET_SURVEYS_FAILED: 'admin/GET_SURVEYS_SUCCEEDED',
  DELETE_SURVEY_REQUESTED: 'admin/DELETE_SURVEY_REQUESTED',
  DELETE_SURVEY_SUCCEEDED: 'admin/DELETE_SURVEY_SUCCEEDED',
  CHANGE_SURVEY_STATE_REQUESTED: 'admin/CHANGE_SURVEY_STATE_REQUESTED',
  CHANGE_SURVEY_STATE_SUCCEEDED: 'admin/CHANGE_SURVEY_STATE_SUCCEEDED',
};

const initialState = immutable({
});

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_SURVEYS_REQUESTED:
      return {
        ...state,
      };
    case actionTypes.GET_SURVEYS_SUCCEEDED:
      return {
        ...state,
        surveys: action.payload,
      };
    case actionTypes.CHANGE_SURVEY_STATE_REQUESTED:
      return {
        ...state,
      };
    default:
      return state;
  }
}