import {
  all, put, call, fork, takeLatest, select,
} from 'redux-saga/effects';
import axios from 'axios';

import { actionTypes } from './reducer';

function* getSurveys() {
  try {
    const state = yield select();
    axios.defaults.headers.common.Authorization = state.login.token || '';
    const response = yield call(axios, '/api/surveys', {
      method: 'GET',
    });
    yield put({ type: actionTypes.GET_SURVEYS_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
  }
}

function* deleteSurvey(action) {
  try {
    const state = yield select();
    axios.defaults.headers.common.Authorization = state.login.token || '';
    const response = yield call(axios, '/api/delete', {
      method: 'POST',
      data: {
        surveyId: action.surveyId,
      }
    });
    yield put({ type: actionTypes.DELETE_SURVEY_SUCCEEDED, payload: response.data });
    yield put({ type: actionTypes.GET_SURVEYS_REQUESTED });
  } catch (e) {
    console.log('-->e', e);
  }
}

function* changeSurveyState(action) {
  try {
    const state = yield select();
    axios.defaults.headers.common.Authorization = state.login.token || '';
    const response = yield call(axios, '/api/state', {
      method: 'POST',
      data: {
        surveyId: action.surveyId,
      }
    });
    yield put({ type: actionTypes.CHANGE_SURVEY_STATE_SUCCEEDED, payload: response.data });
    yield put({ type: actionTypes.GET_SURVEYS_REQUESTED });
  } catch (e) {
    console.log('-->e', e);
  }
}

function* takeGetSurveys() {
  yield takeLatest(actionTypes.GET_SURVEYS_REQUESTED, getSurveys);
}

function* takeDeleteSurvey() {
  yield takeLatest(actionTypes.DELETE_SURVEY_REQUESTED, deleteSurvey);
}

function* takeChangeSurveyState() {
  yield takeLatest(actionTypes.CHANGE_SURVEY_STATE_REQUESTED, changeSurveyState);
}

export default function* () {
  yield all([
    fork(takeGetSurveys),
    fork(takeDeleteSurvey),
    fork(takeChangeSurveyState),
  ]);
}
