import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import { Link } from 'react-router-dom';

import './SurveyCard.scss';

import { changeSurveyState, deleteSurvey } from '../../actions';
import {withStyles} from '@material-ui/core/styles/';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();

  return {
    // top: `300px`,
    // left: `40%`,
    // transform: `translate(-${top}%, -${left}%)`,
    // position: 'absolute',
    margin: '20% auto',
    maxWidth: 400,
    padding: '20px',
    outline: 'none',
  };
}

class SurveyCard extends React.PureComponent {
  state = {
    show: false,
  };

  onDelete = () => {
    // this.openDeleteWindow();
    const { props, props: { survey } } = this;
    props.deleteSurvey(survey.survey_id)
  };

  openDeleteWindow = () => {
    this.setState({show: true})
  };

  closeDeleteWindow = () => {
    this.setState({show: false})
  };

  render() {
    const { props, props: { survey, classes }, state } = this;
    const modalStyle = getModalStyle();
    return (
      <>
        <div className="survey-card-wrapper">
          <Tooltip title="Удалить опрос" placement="top">
            <i onClick={this.openDeleteWindow} className="fas fa-trash" />
          </Tooltip>
          <CopyToClipboard
            text={`${survey.password}`}
            onCopy={() => this.setState({copied: true})}
          >
            <Tooltip title="Получить пароль к опросу" placement="top">
              <i className="fas fa-key" />
            </Tooltip>
          </CopyToClipboard>
          <CopyToClipboard
            text={`${window.location.origin}/survey/${survey.survey_id}`}
          >
            <Tooltip title="Получить ссылку на опрос" placement="top">
              <i className="fas fa-link" />
            </Tooltip>
          </CopyToClipboard>
          <Card className="survey-card">
            <CardContent className="survey-card-content">
              <h4 className="survey-card__tilte">{survey.name.charAt(51) !== '' ? `${survey.name.substr(0, 50)}...` : survey.name}</h4>
              <div className="survey-card__controls">
                <Divider />
                <div className="control-buttons">
                  <span><i className="fas fa-users"></i>{survey.numb_of_res}</span>
                  <div className="control-links">
                    <Link to={`/edit/${survey.survey_id}`}>
                      <Tooltip title="Открыть редактор" placement="top-start">
                        <Button className="survey-card-button__open-editor" variant="outlined" color="primary">
                          <i className="far fa-edit"></i>
                        </Button>
                      </Tooltip>
                    </Link>
                    <Link to={`/results/${survey.survey_id}`}>
                      <Tooltip title="Просмотреть результаты" placement="top">
                        <Button className="survey-card-button__open-results" variant="outlined" color="secondary">
                          <i className="far fa-chart-bar"></i>
                        </Button>
                      </Tooltip>
                    </Link>
                  </div>
                </div>
                <Divider />
                <Switch
                  checked={survey.state}
                  onChange={() => props.changeSurveyState(survey.survey_id)}
                  value={survey.survey_id}
                  classes={{ switchBase: classes.switchBase, checked: classes.checked, track: classes.track, root: classes.root }}
                />
                <Divider />
              </div>
            </CardContent>
          </Card>
        </div>
        <div>
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={state.show}
            onClose={this.closeDeleteWindow}
          >
            <Card style={modalStyle}>
              <h2 id="simple-modal-title">Вы действительно хотите удалить?</h2>
              <div className={classes.buttons}>
                <Button className="cancel-button" onClick={this.onDelete}>Удалить</Button>
                <Button onClick={this.closeDeleteWindow}>Отмена</Button>
              </div>
            </Card>
          </Modal>
        </div>
      </>
    )
  }
}

const styles = () => ({
  buttons: {
    display: 'flex',
    '& .cancel-button': {
      backgroundColor: 'red',
      color: '#ffffff',
      justifyContent: 'center',
    }
  },
  labelButton: {
    display: 'flex',
    flexDirection: 'column',
  },
  root: {
    marginTop: '10px',
    marginBottom: '10px',
  },
  switchBase: {
    height: '20px',
    '&$checked': {
      color: '#32CD32',
      '&$checked + $track': {
        backgroundColor: 'rgba(50, 205, 50, 1)',
      },
    },
  },
  checked: {},
  track: {},
});

const mapDispatchToProps = {
  deleteSurvey,
  changeSurveyState,
};

export default withStyles(styles)(connect(null, mapDispatchToProps)(SurveyCard));