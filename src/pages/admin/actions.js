import { actionTypes } from './reducer';

export const getSurveys = () => ({
  type: actionTypes.GET_SURVEYS_REQUESTED,
});

export const deleteSurvey = (surveyId) => ({
  type: actionTypes.DELETE_SURVEY_REQUESTED,
  surveyId,
});

export const changeSurveyState = (surveyId) => ({
  type: actionTypes.CHANGE_SURVEY_STATE_REQUESTED,
  surveyId,
});
