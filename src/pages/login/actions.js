import { actionTypes } from './reducer';

export const logIn = (payload) => ({
  type: actionTypes.LOG_IN_REQUESTED,
  payload,
});

export const setRedirectToLogin = (payload) => ({
  type: actionTypes.SET_REDIRECT_TO_LOGIN_PAGE,
  payload,
});
