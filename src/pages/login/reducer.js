import immutable from 'seamless-immutable';
import { getCookie } from '../../helper/utils';

export const actionTypes = {
  SET_AUTH_FIELD: 'login/SET_AUTH_FIELD',
  LOG_IN_REQUESTED: 'login/LOG_IN_REQUESTED',
  LOG_IN_SUCCEEDED: 'login/LOG_IN_SUCCEEDED',
  SET_REDIRECT_TO_LOGIN_PAGE: 'login/SET_REDIRECT_TO_LOGIN_PAGE',
};



const initialState = immutable({
  isLogged: !!getCookie('token'),
  token: getCookie('token'),
});

// const cookies = new Cookies();

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_AUTH_FIELD:
      return {
        ...state,
        [action.payload.field]: action.payload.value,
      };
    case actionTypes.LOG_IN_REQUESTED:
      return {
        ...state,
      };
    case actionTypes.LOG_IN_SUCCEEDED:
      return {
        ...state,
        token: action.payload,
        isLogged: true,
      };
    case actionTypes.SET_REDIRECT_TO_LOGIN_PAGE:
      return {
        ...state,
        token: null,
        isLogged: false,
      };
    default:
      return state;
  }
}