import {
  all, put, call, fork, takeLatest,
} from 'redux-saga/effects';
import axios from 'axios';
import { setCookie } from '../../helper/utils';
import { actionTypes } from './reducer';

function* logIn(action) {
  try {
    const response = yield call(axios, '/api/login', {
      method: 'POST',
      data: {
        email: action.payload.email,
        password: action.payload.password,
      },
    });
    setCookie('token', response.data);
    yield put({ type: actionTypes.LOG_IN_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
  }
}

function* takeLogIn() {
  yield takeLatest(actionTypes.LOG_IN_REQUESTED, logIn);
}

export default function* () {
  yield all([
    fork(takeLogIn),
  ]);
}
