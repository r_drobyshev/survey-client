import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import './login.scss';
import LoginPageMain from './LoginPageMain';

import Header from '../../components/SurveyHeader/SurveyHeader';

class LoginPage extends React.PureComponent {
  componentDidMount() {
  }

  render() {
    const { props } = this;
    if (props.isLogged) return <Redirect to="/" />;
    return (
      <>
        <Header title="Опросы ЮФУ" />
        <LoginPageMain />
      </>
    )
  }
}

const mapStateToProps = state => ({
  isLogged: state.login.isLogged,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);