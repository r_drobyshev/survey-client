import React from 'react';

import './login.scss';
import AuthForm from './components/AuthForm/AuthForm';

class LoginPageMain extends React.PureComponent {
  render() {
    return (
      <div className="body-wrapper login-main">
        <AuthForm />
      </div>
    )
  }
}

export default LoginPageMain;