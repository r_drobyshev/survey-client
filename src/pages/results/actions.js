import { actionTypes } from './reducer';

export const getResults = (payload) => ({
  type: actionTypes.GET_RESULTS_BY_ID_REQUESTED,
  payload,
});

export const setActiveBlock = (blockId) => ({
  type: actionTypes.SET_ACTIVE_BLOCK,
  blockId,
});

export const exportResults = (surveyId) => ({
  type: actionTypes.EXPORT_RESULTS_REQUESTED,
  surveyId,
});

export const clearSurveyInfo = () => ({
  type: actionTypes.CLEAR_SURVEY_INFO,
});
