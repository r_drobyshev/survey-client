import { createSelector } from 'reselect';

const getResults = state => state.results.results;
const activeBlock = state => state.results.activeBlock;

const getResultsByBlock = createSelector(
  getResults,
  activeBlock,
  (results, activeBlock) => (
    results && results.filter(result => result.block_id === activeBlock)
  )
);

export default getResultsByBlock;
