import immutable from 'seamless-immutable';

export const actionTypes = {
  GET_RESULTS_BY_ID_REQUESTED: 'results/GET_RESULTS_BY_ID_REQUESTED',
  GET_RESULTS_BY_ID_SUCCEEDED: 'results/GET_RESULTS_BY_ID_SUCCEEDED',
  GET_RESULTS_BY_ID_FAILURE: 'result/GET_RESULTS_BY_ID_FAILURE',
  SET_ACTIVE_BLOCK: 'results/SET_ACTIVE_BLOCK',
  EXPORT_RESULTS_REQUESTED: 'results/EXPORT_RESULTS_REQUESTED',
  EXPORT_RESULTS_SUCCEEDED: 'results/EXPORT_RESULTS_SUCCEEDED',
  EXPORT_RESULTS_FAILURE: 'results/EXPORT_RESULTS_FAILURE',
  CLEAR_SURVEY_INFO: 'results/CLEAR_SURVEY_INFO',
};

const initialState = immutable({
  activeBlock: '0',
  isRequesting: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_RESULTS_BY_ID_REQUESTED:
      return {
        ...state,
        isRequesting: true,
        results: [],
      };
    case actionTypes.GET_RESULTS_BY_ID_SUCCEEDED:
      return {
        ...state,
        survey: action.payload.survey,
        results: action.payload.results,
        totalResults: action.payload.totalResults || null,
        isRequesting: false,
      };
    case actionTypes.GET_RESULTS_BY_ID_FAILURE:
      return {
        isRequesting: false,
      };
    case actionTypes.SET_ACTIVE_BLOCK:
      return {
        ...state,
        activeBlock: action.blockId,
        results: [],
        totalResults: [],
      };
    case actionTypes.CLEAR_SURVEY_INFO:
      return {
        ...state,
        survey: null,
        results: null,
        totalResults: null,
      };
    default:
      return state;
  }
}