import {
  all, put, call, fork, takeLatest, select,
} from 'redux-saga/effects';
import axios from 'axios';
import { saveAs } from 'file-saver';
import XLSX from 'xlsx';
import moment from 'moment';

import { actionTypes } from './reducer';
import { s2ab } from '../../helper/utils';

function* getResultsById(action) {
  try {
    const state = yield select();
    axios.defaults.headers.common.Authorization = state.login.token || '';
    const { surveyId, blockId = null } = action.payload;
    const response = yield call(axios, `/api/results/${surveyId}`, {
      method: 'GET',
      params: { blockId },
    });
    yield put({ type: actionTypes.GET_RESULTS_BY_ID_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
    yield put({ type: actionTypes.GET_RESULTS_BY_ID_FAILURE });
  }
}

function* exportResults(action) {
  try {
    const state = yield select();
    axios.defaults.headers.common.Authorization = state.login.token || '';
    const { surveyId } = action;
    const response = yield call(axios, `/api/results/export`, {
      method: 'GET',
      params: { surveyId },
    });
    yield put({ type: actionTypes.EXPORT_RESULTS_SUCCEEDED, payload: response.data });
    const wb = XLSX.utils.book_new();
    wb.Props = {
      Title: "Results",
      CreatedDate: moment().format(),
    };
    wb.SheetNames.push("All Results");
    const ws_data = [['answer_id' , 'block_id', 'question_text', 'answer']];
    // const ws = XLSX.utils.aoa_to_sheet(response.data);
    console.log('-->response.data', response.data);
    const ws = XLSX.utils.json_to_sheet(response.data);
    wb.Sheets["All Results"] = ws;
    const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)], {type:"application/octet-stream"}), 'results.xlsx');
  } catch (e) {
    console.log('-->e', e);
    yield put({ type: actionTypes.EXPORT_RESULTS_FAILURE });
  }
}

function* takeResultsById() {
  yield takeLatest(actionTypes.GET_RESULTS_BY_ID_REQUESTED, getResultsById);
}

function* takeExportResults() {
  yield takeLatest(actionTypes.EXPORT_RESULTS_REQUESTED, exportResults);
}

export default function* () {
  yield all([
    fork(takeResultsById),
    fork(takeExportResults),
  ]);
}
