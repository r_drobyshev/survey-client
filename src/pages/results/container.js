import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';

import './results.scss';
import ResultsPageMain from './ResultsPageMain';
import { getResults, setActiveBlock, clearSurveyInfo } from './actions';

import AdminHeader from '../../components/AdminHeader/AdminHeader';
import { compareBlocks, deleteCookie } from '../../helper/utils';

class ResultsPage extends React.PureComponent {
  componentDidMount() {
    const { props, props: { match: { params: { id } } } } = this;
    props.getResults({surveyId: id});
    console.log('here_container mounted');
  }

  componentDidUpdate(prevProps) {
    const {
      props,
      props: { match: { params: { id } } },
      props: { activeBlock, survey },
    } = this;
    let blockId = activeBlock;
    if (blockId === '0' && survey) {
      blockId = survey.blocks.sort(compareBlocks)[0];
      props.setActiveBlock(blockId);
    }
    if (prevProps.activeBlock !== activeBlock) {
      console.log('here_getting results............');
      return props.getResults({ blockId: activeBlock, surveyId: id });
    }
  }

  componentWillUnmount() {
    const {
      props,
    } = this;
    props.setActiveBlock('0');
    props.clearSurveyInfo();
  }

  render() {
    const { props } = this;
    if (!props.isLogged) return <Redirect to="/login" />;
    if (!props.results) return null;
    return (
      <>
        <AdminHeader />
        <ResultsPageMain />
      </>
    )
  }
}

const mapStateToProps = state => ({
  results: state.results.results,
  survey: state.results.survey,
  isLogged: state.login.isLogged,
  activeBlock: state.results.activeBlock,
  isRequesting: state.results.isRequesting,
});

const mapDispatchToProps = {
  getResults,
  setActiveBlock,
  clearSurveyInfo,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResultsPage));