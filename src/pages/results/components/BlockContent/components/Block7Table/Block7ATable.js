import React from 'react';
import MUIDataTable from "mui-datatables";
import XLSX from 'xlsx';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { s2ab } from '../../../../../../helper/utils';

export default class Block1Table extends React.PureComponent {
  onDownload = () => {
    const { data } = this.props;
    const wb = XLSX.utils.book_new();
    wb.Props = {
      Title: "Block7AResults",
      CreatedDate: moment().format(),
    };
    wb.SheetNames.push("Block7AResults");
    const ws_data = ['Респондент №', 'Интегральный показатель'];
    const data2 = data && data.integralResults && data.integralResults.length && data.integralResults.map(item => {
      return {
        [ws_data[0]]: item.respondent,
        [ws_data[1]]: item.total,
      }
    });
    const ws = XLSX.utils.json_to_sheet(data2, { header: ws_data, });
    function fitToColumn(data2) {
      // get maximum character of each column
      return Object.entries(data2[0]).map(([ key, value ]) => ({ wch: Math.max(String(key).length, String(value).length) }));
    }
    ws['!cols'] = fitToColumn(data2);
    wb.Sheets["Block7AResults"] = ws;
    const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)], {type:"application/octet-stream"}), 'results7ABlock.xlsx');
    return false;
  };
  render() {
    const options = {
      // filterType: "dropdown",
      responsive: "scrollMaxHeight",
      onDownload: this.onDownload,
    };
    const { data } = this.props;
    console.log('-->data', data);
    const columns = [
      'Респондент №',
      'Интегральный показатель',
    ];
    if (!data.integralResults) return null;
    return (
      <MUIDataTable
        data={data && data.integralResults.map((item) => {
          return [
            item.respondent,
            item.total,
          ]
        })}
        columns={columns}
        options={options}
      />
    );
  }
}