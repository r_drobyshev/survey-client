import React from 'react';
import MUIDataTable from "mui-datatables";
import XLSX from 'xlsx';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { s2ab } from '../../../../../../helper/utils';

export default class Block1Table extends React.PureComponent {
  onDownload = () => {
    const { data } = this.props;
    const wb = XLSX.utils.book_new();
    wb.Props = {
      Title: "Block7BResults",
      CreatedDate: moment().format(),
    };
    wb.SheetNames.push("Block7BResults");
    const ws_data = [
      '№ вопроса',
      'Среднее',
      '%1',
      '%2',
      '%3',
      '%4',
      '%5',
    ];
    const data2 = data && data.raspredResults && data.raspredResults.length && data.raspredResults.map((item, index) => {
      return {
        [ws_data[0]]: index + 1,
        [ws_data[1]]: item.average,
        [ws_data[2]]: item.group_1,
        [ws_data[3]]: item.group_2,
        [ws_data[4]]: item.group_3,
        [ws_data[5]]: item.group_4,
        [ws_data[6]]: item.group_5,
      }
    });
    const ws = XLSX.utils.json_to_sheet(data2, { header: ws_data, });
    function fitToColumn(data2) {
      // get maximum character of each column
      return Object.entries(data2[0]).map(([ key, value ]) => ({ wch: Math.max(String(key).length, String(value).length) }));
    }
    ws['!cols'] = fitToColumn(data2);
    wb.Sheets["Block7BResults"] = ws;
    const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)], {type:"application/octet-stream"}), 'results7BBlock.xlsx');
    return false;
  };
  render() {
    const options = {
      // filterType: "dropdown",
      responsive: "scrollMaxHeight",
      onDownload: this.onDownload,
    };
    const { data } = this.props;
    const columns = [
      '№ вопроса',
      'Среднее',
      '%1',
      '%2',
      '%3',
      '%4',
      '%5',
    ];
    if (!data.raspredResults) return null;
    return (
      <MUIDataTable
        data={data && data.raspredResults.map((item, index) => {
          return [
            index + 1,
            item.average,
            item.group_1,
            item.group_2,
            item.group_3,
            item.group_4,
            item.group_5,
          ]
        })}
        columns={columns}
        options={options}
      />
    );
  }
}