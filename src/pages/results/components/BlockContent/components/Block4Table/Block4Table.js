import React from 'react';
import MUIDataTable from "mui-datatables";
import XLSX from 'xlsx';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { s2ab } from '../../../../../../helper/utils';

export default class Block1Table extends React.PureComponent {
  onDownload = () => {
    const { data } = this.props;
    const wb = XLSX.utils.book_new();
    wb.Props = {
      Title: "Block4Results",
      CreatedDate: moment().format(),
    };
    wb.SheetNames.push("Block4Results");
    const ws_data = [
      'Респондент №',
      'Общий интегральный показатель (Престижно-материальные мотивы)',
      'Общий интегральный показатель (Мотивы самореализации)',
      'Общий интегральный показатель (Мотивы построения карьеры)',
      'Общий интегральный показатель (Формальные мотивы и мотивы «легкой жизни»)',
    ];
    const data2 = data && data.length && data.map(item => {
      return {
        [ws_data[0]]: item.respondent,
        [ws_data[1]]: item.totalA,
        [ws_data[2]]: item.totalB,
        [ws_data[3]]: item.totalC,
        [ws_data[4]]: item.totalD,
      }
    });
    const ws = XLSX.utils.json_to_sheet(data2, { header: ws_data, });
    function fitToColumn(data2) {
      // get maximum character of each column
      return Object.entries(data2[0]).map(([ key, value ]) => ({ wch: Math.max(String(key).length, String(value).length) }));
    }
    ws['!cols'] = fitToColumn(data2);
    wb.Sheets["Block4Results"] = ws;
    const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)], {type:"application/octet-stream"}), 'results4Block.xlsx');
    return false;
  };
  render() {
    const options = {
      // filterType: "dropdown",
      responsive: "scrollMaxHeight",
      onDownload: this.onDownload,
    };
    const { data } = this.props;
    const columns = [
      'Респондент №',
      'Общий интегральный показатель (Престижно-материальные мотивы)',
      'Общий интегральный показатель (Мотивы самореализации)',
      'Общий интегральный показатель (Мотивы построения карьеры)',
      'Общий интегральный показатель (Формальные мотивы и мотивы «легкой жизни»)',
    ];
    if (!data.length) return null;
    return (
      <MUIDataTable
        data={data && data.length && data.map(item => {
          return [
            item.respondent,
            item.totalA,
            item.totalB,
            item.totalC,
            item.totalD,
          ]
        })}
        columns={columns}
        options={options}
      />
    );
  }
}