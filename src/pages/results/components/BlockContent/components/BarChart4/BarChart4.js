import React from 'react'
import Highcharts from 'highcharts';
import HC_more from 'highcharts/modules/exporting';
import HighchartsReact from 'highcharts-react-official';

import './BarChart.scss';

HC_more(Highcharts);

function getData2(result) {
  const cats = result.map(result => result.title);
  const data = result.map(result => result.total);
  return cats.map((cat, index) => {
    return {
      data: [data[index]],
      name: cat,
    };
  })
}

export default class BarChart4 extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      chartOptions: {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Мотивы поступления в вуз',
        },
        legend: {
          align: 'left',
          verticalAlign: 'bottom',
          layout: 'horizontal',
        },
        xAxis: {
          type: 'category',
          // categories: [props.data.map(data => data.title), 0, 1],
        },
        series: getData2(props.data),
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              //chartOptions
            }
          }]
        }
      },
      isEditing: false,
    };
  }
  render() {
    const { chartOptions } = this.state;
    const { props } = this;
    return (
      <div className="react-chart">
        <HighchartsReact
          highcharts={Highcharts}
          options={chartOptions}
        />
      </div>
    )
  }
}