import React from 'react'
import Highcharts from 'highcharts';
import HC_more from 'highcharts/modules/exporting';
import HighchartsReact from 'highcharts-react-official';

import './BarChart.scss';

HC_more(Highcharts);

function getData2(result) {
  const cats = JSON.parse(result.answer_variants).filter((cat, index) => Object.keys(result.totalCount).some(key => index + 1 === +key) ? cat : false);
  const data = Object.values(result.totalCount);
  return cats.map((cat, index) => {
    return {
      data: [data[index]],
      name: [cat],
    };
  })
}

export default class BarChart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      chartOptions: {
        chart: {
          type: 'column'
        },
        title: {
          text: props.data.text,
        },
        legend: {
          align: 'left',
          verticalAlign: 'bottom',
          layout: 'horizontal',
        },
        xAxis: {
          categories: [props.data.text],
        },
        series: getData2(props.data),
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              //chartOptions
            }
          }]
        }
      },
      isEditing: false,
    };
  }
  render() {
    const { chartOptions } = this.state;
    const { props } = this;
    return (
      <div className="react-chart">
        <HighchartsReact
          highcharts={Highcharts}
          options={chartOptions}
        />
      </div>
    )
  }
}