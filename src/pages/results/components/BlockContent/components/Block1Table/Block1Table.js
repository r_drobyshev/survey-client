import React from 'react';
import MUIDataTable from "mui-datatables";
import XLSX from 'xlsx';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { s2ab } from '../../../../../../helper/utils';

export default class Block1Table extends React.PureComponent {
  onDownload = () => {
    const { data } = this.props;
    const wb = XLSX.utils.book_new();
    wb.Props = {
      Title: "Block1Results",
      CreatedDate: moment().format(),
    };
    wb.SheetNames.push("Block1Results");
    const ws_data = data.length && [...Object.values(data[0]).map(key => key.question)];
    const data2 = data && data.length && data.map(item => {
      return {
        [ws_data[0]]: item.respondent.number,
        [ws_data[1]]: item.question_1.answer,
        [ws_data[2]]: item.question_2.answer,
        [ws_data[3]]: item.question_3.answer,
        [ws_data[4]]: item.question_4.answer,
        [ws_data[5]]: item.question_5.answer,
        [ws_data[6]]: item.question_6.answer,
        [ws_data[7]]: item.question_7.answer,
        [ws_data[8]]: item.question_8.answer,
        [ws_data[9]]: item.question_9.answer,
    }
    });
    const ws = XLSX.utils.json_to_sheet(data2, { header: ws_data, });

    function fitToColumn(data2) {
      // get maximum character of each column
      return Object.entries(data2[0]).map(([ key, value ]) => ({ wch: Math.max(String(key).length, String(value).length) }));
    }
    ws['!cols'] = fitToColumn(data2);
    console.log('-->ws[\'!cols\']', ws['!cols']);
    wb.Sheets["Block1Results"] = ws;
    const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)], {type:"application/octet-stream"}), 'results1Block.xlsx');
    return false;
  };
  render() {
    const options = {
      // filterType: "dropdown",
      responsive: "scrollMaxHeight",
      onDownload: this.onDownload,
    };
    const { data } = this.props;
    const ws_data = data.length && [...Object.values(data[0]).map(key => key.question)];
    const data2 = data && data.length && data.map(item => {
      return {
        [ws_data[0]]: item.respondent.number,
        [ws_data[1]]: item.question_1.answer,
        [ws_data[2]]: item.question_2.answer,
        [ws_data[3]]: item.question_3.answer,
        [ws_data[4]]: item.question_4.answer,
        [ws_data[5]]: item.question_5.answer,
        [ws_data[6]]: item.question_6.answer,
        [ws_data[7]]: item.question_7.answer,
        [ws_data[8]]: item.question_8.answer,
        [ws_data[9]]: item.question_9.answer,
      }
    });
    console.log('-->data2', data2);
    const columns = data.length && Object.values(data[0]).map(key => key.question);

    return (
      <MUIDataTable
        data={data && data.length && data.map(item => {
          return [
            item.respondent && item.respondent.number,
            item.question_1 && item.question_1.answer,
            item.question_2 && item.question_2.answer,
            item.question_3 && item.question_3.answer,
            item.question_4 && item.question_4.answer,
            item.question_5 && item.question_5.answer,
            item.question_6 && item.question_6.answer,
            item.question_7 && item.question_7.answer,
            item.question_8 && item.question_8.answer,
            item.question_9 && item.question_9.answer,
          ]
        })}
        columns={columns}
        options={options}
      />
    );
  }
}