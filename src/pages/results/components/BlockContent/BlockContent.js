import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import classNames from 'classnames';

import './BlockContent.scss';
import BarChart from './components/BarChart/BarChart';
import BarChart4 from './components/BarChart4/BarChart4';

// import getResultsByBlock from '../../selectors';
import { exportResults } from '../../actions';
import Block1Table from "./components/Block1Table/Block1Table";
import Block3Table from "./components/Block3Table/Block3Table";
import Block4Table from "./components/Block4Table/Block4Table";
import Block5Table from "./components/Block5Table/Block5Table";
import Block7ATable from "./components/Block7Table/Block7ATable";
import Block7BTable from "./components/Block7Table/Block7BTable";
import Block8Table from "./components/Block8Table/Block8Table";
import Block9Table from "./components/Block9Table/Block9Table";
import Block10ATable from "./components/Block10Table/Block10ATable";
import Block10BTable from "./components/Block10Table/Block10BTable";

function createData(name, calories) {
  return { name, calories };
}

class BlockContent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false,
      isBlockSelected: false,
    };
  }

  componentDidUpdate() {
    this.setState({
      isBlockSelected: true,
    });
    const { props } = this;
    if (props.file) {
    }
  }

  componentWillUnmount() {
    this.setState({
      isBlockSelected: false,
    })
  }

  exportResults = () => {
    const { props } = this;
    props.exportResults(props.match.params.id);
  };

  render() {
    const { props, state } = this;
    const contentWrapperStyle = classNames({
      'full-width': props.results && !props.results.length && Array.isArray(props.results),
    });
    const loader = {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      fontSize: '28px',
    };
    if (props.isRequesting) return <span style={loader}><i className="fas fa-spin fa-spinner" /></span>;
    return (
      <div className={`results-content ${contentWrapperStyle}`}>
        <div className="top-elements">
        </div>
        <div className="title-content">
          <h2 className='survey__title'>{props.survey.name}</h2>
        </div>
        <div className="control-buttons-wrapper">
          <Button disabled={!props.results.length && Array.isArray(props.results)} onClick={this.exportResults} className="survey-results__export-button" variant="contained">
            Экспорт всей анкеты
            <i className="fas fa-download"></i>
          </Button>
          {/*<Button disabled={!props.results.length && Array.isArray(props.results)} onClick={this.exportResults} className="survey-results__export-block-button" variant="contained">*/}
            {/*Экспорт текущего блока*/}
            {/*<i className="fas fa-download"></i>*/}
          {/*</Button>*/}
        </div>
        {
          !props.results.length && props.activeBlock !== '7' && props.activeBlock !== '10' && (
            <Card>
              <CardContent>
                Для просмотра статистики необходимо хотя бы один раз пройти опрос!
              </CardContent>
            </Card>
          )
        }
        <>
          {
            props.activeBlock === '1' && !!props.results.length && (
              <>
                <Block1Table data={props.results} />
              </>
            )
          }
          {
            props.activeBlock === '2' && Array.isArray(props.results) && props.results.map(result => {
              if (result.totalCount && result.type !== 'number') {
                return (
                  <BarChart key={result.question_id} data={result}/>
                )
              }
              return null
            })
          }
          {
            props.activeBlock === '3' && (
              <>
                <Block3Table data={props.results} />
              </>
            )
          }
          {
            props.activeBlock === '4' && !!props.results.length && (
              <>
                <Block4Table data={props.results} />
                {
                  props.totalResults ? (
                    <BarChart4 data={props.totalResults} />
                  ) : null
                }
              </>
            )
          }
          {
            props.activeBlock === '5.1' && (
              <>
                <Block5Table data={props.results} />
                {/*{*/}
                  {/*props.totalResults ? (*/}
                    {/*<BarChart5 data={props.results} />*/}
                  {/*) : null*/}
                {/*}*/}
              </>
            )
          }
          {
            props.activeBlock === '5.2' && (
              <>
                <Block5Table data={props.results} />
                {/*{*/}
                  {/*props.totalResults ? (*/}
                    {/*<BarChart5 data={props.results} />*/}
                  {/*) : null*/}
                {/*}*/}
              </>
            )
          }
          {
            props.activeBlock === '6' && (
              <>
                {
                  props.results && Array.isArray(props.results) && props.results.map(result => {
                    if (result.totalCount && result.type !== 'number') {
                      return (
                        <BarChart key={result.question_id} data={result}/>
                      )
                    }
                    return null
                  })
                }
              </>
            )
          }
          {
            props.activeBlock === '7' && props.results.integralResults && (
              <>
                <h1>По респонденту</h1>
                <Block7ATable data={props.results} />
                <h1>По выборке</h1>
                <Block7BTable data={props.results} />
                {/*{*/}
                  {/*props.totalResults ? (*/}
                    {/*<BarChart5 data={props.results} />*/}
                  {/*) : null*/}
                {/*}*/}
              </>
            )
          }
          {
            props.activeBlock === '8.1' && !!props.results.length && (
              <>
                <Block8Table data={props.results} />
                {
                  props.totalResults ? (
                    <BarChart4 data={props.totalResults} />
                  ) : null
                }
              </>
            )
          }
          {
            props.activeBlock === '8.2' && !!props.results.length && (
              <>
                <Block8Table data={props.results} />
                {
                  props.totalResults ? (
                    <BarChart4 data={props.totalResults} />
                  ) : null
                }
              </>
            )
          }
          {
            props.activeBlock === '9' && !!props.results.length && (
              <>
                <Block9Table data={props.results} />
                {
                  props.totalResults ? (
                    <BarChart4 data={props.totalResults} />
                  ) : null
                }
              </>
            )
          }
          {
            props.activeBlock === '10' && props.results.aResults && (
              <>
                <h1>По респонденту</h1>
                <Block10ATable data={props.results} />
                <h1>По выборке</h1>
                <Block10BTable data={props.results} />
              </>
            )
          }
        </>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  // resultsByBlock: getResultsByBlock(state),
  activeBlock: state.results.activeBlock,
  results: state.results.results,
  totalResults: state.results.totalResults,
  survey: state.results.survey,
  file: state.results.file,
  isRequesting: state.results.isRequesting,
});

const mapDispatchToProps = {
  exportResults,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BlockContent));
