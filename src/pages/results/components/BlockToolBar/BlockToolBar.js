import React from 'react';
import { connect } from "react-redux";

import './BlockToolBar.scss';
import { setActiveBlock, getResults } from '../../actions';
import { compareBlocks } from '../../../../helper/utils';

class BlockToolBar extends React.PureComponent {
  state = {};

  handleClick = (e) => {
    const blockId = e.target.name.replace('block_', '') || 0;
    console.log('-->blockId-onClick', blockId);
    const { props, props: { activeBlock } }= this;
    if (activeBlock === blockId) return;
    return props.setActiveBlock(blockId);
  };

  render() {
    const { props } = this;
    // if (!props.results.length) return null;
    // let blocks = props.results && props.results.map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index));
    // console.log('-->blocks', blocks);
    // console.log('-->props.survey.blocks', props.survey.blocks);
    const blocks = props.survey && props.survey.blocks.sort(compareBlocks);
    if (!blocks) return null;
    return (
      <div className="result-blocks">
        {
          blocks.map(item => (
            <button className="block-button" key={item.toString()} type="button" name={`block_${item}`} onClick={this.handleClick}>
              Блок №{item}
            </button>
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  results: state.results.results,
  survey: state.results.survey,
  activeBlock: state.results.activeBlock,
});

const mapDispatchToProps = {
  setActiveBlock,
  getResults,
};

export default connect(mapStateToProps, mapDispatchToProps)(BlockToolBar);