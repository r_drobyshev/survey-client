import React from 'react';

import './results.scss';

import BlockToolBar from './components/BlockToolBar/BlockToolBar';
import BlockContent from './components/BlockContent/BlockContent';

export default class ResultsPage extends React.PureComponent {
  render() {
    return (
      <div className="body-wrapper results-main">
        <BlockToolBar/>
        <BlockContent />
      </div>
    )
  }
}