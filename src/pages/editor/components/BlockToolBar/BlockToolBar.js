import React from 'react';
import Switch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";

import './BlockToolBar.scss';
import { setActiveBlock, toggleStateOfBlock } from '../../actions';
import { compareBlocks } from '../../../../helper/utils';

const styles = () => ({
  labelButton: {
    display: 'flex',
    flexDirection: 'column',
  },
  root: {
    marginTop: '5px',
  },
  colorSwitchBase: {
    height: '20px',
    '&$colorChecked': {
      color: '#32CD32',
      '& + $colorBar': {
        background: 'linear-gradient(135deg, rgba(73,221,216,1) 0%,rgba(25,226,115,1) 100%);',
      },
    },
  },
  colorChecked: {},
  colorBar: {},
});

class BlockToolBar extends React.PureComponent {
  state = {};

  handleClick = (e) => {
    const blockId = e.target.name.replace('block_', '') || 0;
    const { props } = this;
    props.setActiveBlock(blockId);
  };

  handleChange = name => event => {
    const { target: { checked } } = event;
    const { props } = this;
    props.toggleStateOfBlock({ name, checked })
  };

  handleSwitch = (e) => {
    e.stopPropagation();
  };

  render() {
    const { classes } = this.props;
    const { props } = this;
    let blocks = props.questions.map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index));
    return (
      <div className="editor-blocks">
        {
          blocks.map(item => (
            <button key={item.toString()} id={item.toString()} type="button" name={`block_${item}`} className={`block-button ${item.toString() === props.activeBlock ? 'active' : ''}`} onClick={this.handleClick}>
              Блок №{item}
              {
                props.router.params.id === "0" ? (
                  <Switch
                    checked={props.checkedBlocks[`block_${item}`]}
                    onChange={this.handleChange(`block_${item}`)}
                    value={props.checkedBlocks[`block_${item}`]}
                    classes={{ switchBase: classes.colorSwitchBase, checked: classes.colorChecked, bar: classes.colorBar, root: classes.root }}
                    onClick={this.handleSwitch}
                  />
                ) : null
              }
            </button>
          ) )
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  questions: state.editor.questions,
  checkedBlocks: state.editor.checkedBlocks,
  activeBlock: state.editor.activeBlock,
});

const mapDispatchToProps = {
  setActiveBlock,
  toggleStateOfBlock,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(BlockToolBar));