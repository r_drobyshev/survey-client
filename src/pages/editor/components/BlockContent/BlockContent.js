import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withRouter } from 'react-router';
import Tooltip from '@material-ui/core/Tooltip';

import './BlockContent.scss';
import Question from './components/Question/Question';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import getQuestionsByBlock from '../../selectors';
import { createSurvey, saveSurvey, changeSurveyTitle } from '../../actions';

class BlockContent extends React.PureComponent {
  state = {
    isEditing: false,
  };

  handleSaveOrCreateSurvey = () => {
    const { props } = this;
    if (props.match.params.id === '0') {
      const checkedBlocksArray = [];
      Object.keys(props.checkedBlocks).forEach((key) => {
        if (props.checkedBlocks[key]) checkedBlocksArray.push(key)
      });
      if (checkedBlocksArray.length > 0) {
        props.createSurvey({ questions: props.questions, survey: props.survey, blocks: props.checkedBlocks });
      } else alert("Добавьте хотя бы 1 блок!");
    }
    else props.saveSurvey({ questions: props.questions, survey: props.survey });
  };

  handleOpenLabelInput = async() => {
    await this.setState({ isEditing: true });
    const { labelInput } = this;
    labelInput.focus();
  };

  handleCloseLabelInput = () => {
    this.setState({ isEditing: false });
  };

  handleChangeTitle = (e) => {
    const { props } = this;
    const { target: { value } } = e;
    props.changeSurveyTitle(value);
  };

  handleCopyLink = () => {
    const inputEl = document.querySelector('.survey-link');
    const inputValue = inputEl.innerText.trim();
    if (inputValue) {
      navigator.clipboard.writeText(inputValue)
        .then(() => {
          console.log('Copied!');
        })
        .catch(err => {
          console.log('Something went wrong', err);
        })
    }
  };

  handleCopyPassword = () => {
    const inputEl = document.querySelector('.survey-password');
    const inputValue = inputEl.innerText.trim();
    if (inputValue) {
      navigator.clipboard.writeText(inputValue)
        .then(() => {
          console.log('Copied!');
        })
        .catch(err => {
          console.log('Something went wrong', err);
        })
    }
  };

  render() {
    const { props, state } = this;
    const editorToggler = classNames({
      'show-title': !state.isEditing,
      'show-text-field': state.isEditing,
    });
    return (
      <div className="editor-content">
        <div className="title-content">
          <span>Введите название опроса:</span>
          <h2 className={`survey__title ${editorToggler}`} onClick={this.handleOpenLabelInput}>{props.survey.name}</h2>
          <TextField
            value={props.survey.name}
            id={props.survey.survey_id}
            onChange={this.handleChangeTitle}
            className={`survey__text-field ${editorToggler}`}
            inputRef={(input) => this.labelInput = input}
            onBlur={this.handleCloseLabelInput}
            placeholder=""
            fullWidth
          />
        </div>
        <div className="top-elements">
          {
            props.match.params.id === '0' ? (
              <Button onClick={this.handleSaveOrCreateSurvey} variant="contained" className="editor-button__save">Создать</Button>
            ) : <Button onClick={this.handleSaveOrCreateSurvey} variant="contained" className="editor-button__save">Сохранить</Button>
          }
        </div>
        {
          props.questionsByBlock.map((question, questionIndex) => {
            return (
              <Question key={question.question_id} question={question} questionIndex={questionIndex}/>
            )
          })
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  questionsByBlock: getQuestionsByBlock(state),
  questions: state.editor.questions,
  checkedBlocks: state.editor.checkedBlocks,
  survey: state.editor.survey,
});

const mapDispatchToProps = {
  createSurvey,
  saveSurvey,
  changeSurveyTitle,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BlockContent));