import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import classNames from 'classnames';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { connect } from 'react-redux';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Favorite from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';

import './Question.scss';
import { changeTitle, changeAnswer } from '../../../../actions';

class Question extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state={
      value: 'radio1+1',
      isEditing: false,
      isEditingAnswer: false,
      title: props.question.text,
    };
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleChangeRadio = event => {
    this.setState({ value: event.target.value });
  };

  handleOpenLabelInput = async() => {
    await this.setState({ isEditing: true });
    const { labelInput } = this;
    labelInput.focus();
  };

  handleCloseLabelInput = () => {
    this.setState({ isEditing: false });
  };

  handleChangeTitle = (e) => {
    const { props } = this;
    props.changeTitle({ title: e.target.value, id: e.target.id })
  };

  handleOpenAnswerInput = async() => {
    await this.setState({ isEditingAnswer: true });
    const { answerInput } = this;
    await answerInput.focus();
  };

  handleCloseAnswerInput = () => {
    this.setState({ isEditingAnswer: false });
  };

  handleChangeAnswer = (e) => {
    const { props } = this;
    props.changeAnswer({ answers: e.target.value, id: e.target.id });
  };

  render() {
    const { state, props, props: { classes } } = this;
    const editorToggler = classNames({
      'show-title': !state.isEditing,
      'show-text-field': state.isEditing,
    });
    const editorAnswerToggler = classNames({
      'show-answer': !state.isEditingAnswer,
      'show-answer-text-field': state.isEditingAnswer,
    });
    return (
      <Card className="question-card editor" id={props.question.question_id}>
        <CardContent>
          <h4 className={`question-card__title ${editorToggler}`} onClick={this.handleOpenLabelInput}><b>{`${props.questionIndex+1}. `}</b>{props.question.text}</h4>
          <TextField
            value={props.question.text}
            id={props.question.question_id}
            onChange={this.handleChangeTitle}
            className={`question-card__text-field ${editorToggler}`}
            inputRef={(input) => this.labelInput = input}
            onBlur={this.handleCloseLabelInput}
            placeholder=""
            fullWidth
          />
          {
            props.question.type === 'multi' && (
              <>
                <FormGroup
                  onClick={this.handleOpenAnswerInput}
                >
                  {
                    JSON.parse(props.question.answer_variants).map((answer, answerIndex) => {
                      return (
                        <FormControlLabel
                          id={answer}
                          key={answer}
                          classes={{label: classes.labelFont}}
                          className={`question-card__answer ${editorAnswerToggler}`}
                          control={
                            <Checkbox
                              defaultChecked={false}
                              onChange={this.handleChange(`checked${props.questionIndex + 1}+${answerIndex + 1}`)}
                              value={answer}
                            />
                          }
                          label={answer}
                        />
                      )
                    })
                  }
                </FormGroup>
                <TextField
                  value={JSON.parse(props.question.answer_variants).join('\n')}
                  id={props.question.question_id}
                  onChange={this.handleChangeAnswer}
                  className={`question-card__answer-text-field ${editorAnswerToggler}`}
                  inputRef={(input) => this.answerInput = input}
                  onBlur={this.handleCloseAnswerInput}
                  placeholder=""
                  fullWidth
                  multiline
                />
              </>
            )
          }
          {
            props.question.type === 'single' && (
              <>
                <RadioGroup
                  name={props.question.question_id}
                  value={this.state.value}
                  onClick={this.handleOpenAnswerInput}
                >
                  {
                    JSON.parse(props.question.answer_variants).map((answer, answerIndex) => (
                      <FormControlLabel
                        className={`question-card__answer ${editorAnswerToggler}`}
                        key={`radio1+${answerIndex + 1}`}
                        value={`radio1+${answerIndex + 1}`}
                        control={<Radio />}
                        label={answer}
                        onChange={this.handleChangeRadio}
                      />
                    ))
                  }
                </RadioGroup>
                <TextField
                  value={JSON.parse(props.question.answer_variants).join('\n')}
                  id={props.question.question_id}
                  onChange={this.handleChangeAnswer}
                  className={`question-card__answer-text-field ${editorAnswerToggler}`}
                  inputRef={(input) => this.answerInput = input}
                  onBlur={this.handleCloseAnswerInput}
                  placeholder=""
                  fullWidth
                  multiline
                />
              </>
            )
          }
          {
            props.question.type === 'grade' && (
              <>
                <RadioGroup
                  className="grade-radio-group"
                  name={props.question.question_id}
                  value={this.state.value}
                  onClick={this.handleOpenAnswerInput}
                >
                  {
                    JSON.parse(props.question.answer_variants).map((answer, answerIndex) => (
                      <FormControlLabel
                        className={`question-card__answer ${editorAnswerToggler}`}
                        // className='question-card__answer'
                        key={`radio1+${answerIndex + 1}`}
                        value={`${answerIndex + 1}`}
                        control={<Radio
                          icon={<FavoriteBorder fontSize="large" />}
                          checkedIcon={<Favorite fontSize="large" />}
                        />}
                        label={answer}
                        onChange={this.handleChangeRadio}
                      />
                    ))
                  }
                </RadioGroup>
                <TextField
                  value={JSON.parse(props.question.answer_variants).join('\n')}
                  id={props.question.question_id}
                  onChange={this.handleChangeAnswer}
                  className={`question-card__answer-text-field ${editorAnswerToggler}`}
                  inputRef={(input) => this.answerInput = input}
                  onBlur={this.handleCloseAnswerInput}
                  placeholder=""
                  fullWidth
                  multiline
                />
              </>
            )
          }
        </CardContent>
      </Card>
    )
  }
}

const styles = () => ({
  labelFont: {
    fontFamily: 'Ubuntu, sans-serif',
  }
});

const mapStateToProps = state => ({
  questions: state.editor.questions,
});

const mapDispatchToProps = {
  changeTitle,
  changeAnswer,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Question));