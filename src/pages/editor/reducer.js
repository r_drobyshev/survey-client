import immutable from 'seamless-immutable';

export const actionTypes = {
  GET_SURVEY_BY_ID_REQUESTED: 'editor/GET_SURVEY_BY_ID_REQUESTED',
  GET_SURVEY_BY_ID_SUCCEEDED: 'editor/GET_SURVEY_BY_ID_SUCCEEDED',
  SET_ACTIVE_BLOCK: 'editor/SET_ACTIVE_BLOCK',
  TOGGLE_STATE_OF_BLOCK: 'editor/TOGGLE_STATE_OF_BLOCK',
  CHANGE_TITLE: 'editor/CHANGE_TITLE',
  CHANGE_ANSWER: 'editor/CHANGE_ANSWER',
  CREATE_SURVEY_REQUESTED: 'editor/CREATE_SURVEY_REQUESTED',
  CREATE_SURVEY_SUCCEEDED: 'editor/CREATE_SURVEY_SUCCEEDED',
  SAVE_SURVEY: 'editor/SAVE_SURVEY',
  SAVE_SURVEY_SUCCEEDED: 'editor/SAVE_SURVEY_SUCCEEDED',
  CHANGE_SURVEY_TITLE: 'editor/CHANGE_SURVEY_TITLE',
  RESET_CREATED_STATE: 'editor/RESET_CREATED_STATE',
};

const initialState = immutable({
  checkedBlocks: {
    // block_1: false,
    // block_2: false,
    // block_3: false,
    // block_4: false,
    // block_5: false,
    // block_6: false,
    // block_7: false,
    // block_8: false,
    // block_9: false,
    // block_10: false,
  },
  activeBlock: '1',
  questions: null,
  created: false,
});

function changeTitle(questions, payload) {
  const questionsNew = questions.map(question => {
    if(question.question_id === payload.id) {
      return {
        ...question,
        text: payload.title,
      };
    }
    return {...question};
  });
  return questionsNew;
}

function changeAnswer(questions, payload) {
  const answersArr = payload.answers.split('\n');
  const questionsNew = questions.map(question => {
    if(question.question_id === payload.id) {
      return {
        ...question,
        answer_variants: JSON.stringify(answersArr),
      };
    }
    return {...question};
  });
  return questionsNew;
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_SURVEY_BY_ID_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };
    case actionTypes.GET_SURVEY_BY_ID_SUCCEEDED:
      return {
        ...state,
        questions: action.payload.questions,
        survey: action.payload.survey,
      };
    case actionTypes.SET_ACTIVE_BLOCK:
      return {
        ...state,
        activeBlock: action.blockId,
      };
    case actionTypes.TOGGLE_STATE_OF_BLOCK:
      return {
        ...state,
        checkedBlocks: {
          ...state.checkedBlocks,
          [action.payload.name]: action.payload.checked,
        },
      };
    case actionTypes.CHANGE_TITLE:
      return {
        ...state,
        questions: changeTitle(state.questions, action.payload),
      };
    case actionTypes.CHANGE_ANSWER:
      return {
        ...state,
        questions: changeAnswer(state.questions, action.payload),
      };
    case actionTypes.CREATE_SURVEY_REQUESTED:
      return {
        ...state,
      };
    case actionTypes.CREATE_SURVEY_SUCCEEDED:
      alert('Created!');
      return {
        ...state,
        created: true,
      };
    case actionTypes.SAVE_SURVEY:
      alert('Saved!');
      return {
        ...state,
      };
    case actionTypes.RESET_CREATED_STATE:
      return {
        ...state,
        created: false,
        checkedBlocks: {},
      };
    case actionTypes.CHANGE_SURVEY_TITLE:
      return {
        ...state,
        survey: {
          ...state.survey,
          name: action.payload,
        },
      };
    default:
      return state;
  }
};
