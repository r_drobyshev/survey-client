import React from 'react';

import './editor.scss';
import BlockContent from "./components/BlockContent/BlockContent";
import BlockToolBar from "./components/BlockToolBar/BlockToolBar";

class EditorPageMain extends React.PureComponent {
  render() {
    const { props } = this;
    return (
      <div className="body-wrapper editor-main">
        <BlockToolBar router={props.router} />
        <BlockContent />
      </div>
    )
  }
}

export default EditorPageMain;