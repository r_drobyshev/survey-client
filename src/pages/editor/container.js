import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import { withRouter } from 'react-router';

import './editor.scss';
import { getSurveyById, resetCreatedState, setActiveBlock } from './actions';
import EditorPageMain from './EditorPageMain';

import { compareBlocks } from '../../helper/utils';
import AdminHeader from '../../components/AdminHeader/AdminHeader';

class AdminPage extends React.PureComponent {
  componentDidMount() {
    const { props, props: { match: { params: { id } } } } = this;
    props.getSurveyById(id);
  }

  componentDidUpdate() {
    const { props } = this;
    if (props.questions) {
      const blockId = props.questions.map(question => question.block_id).sort(compareBlocks)[0];
      props.setActiveBlock(blockId);
    }
  }

  componentWillUnmount() {
    const { props } = this;
    props.resetCreatedState();
  }

  render() {
    const { props } = this;
    if (!props.isLogged) return <Redirect to="/login" />;
    if (!props.questions) return null;
    if(props.created) return <Redirect to="/"/>;
    return (
      <>
        <AdminHeader />
        <EditorPageMain router={props.match} />
      </>
    )
  }
}

const mapStateToProps = state => ({
  questions: state.editor.questions,
  created: state.editor.created,
  isLogged: state.login.isLogged,
});

const mapDispatchToProps = {
  getSurveyById,
  resetCreatedState,
  setActiveBlock,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminPage));