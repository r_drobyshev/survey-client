import { actionTypes } from './reducer';

export const getSurveyById = (surveyId) => ({
  type: actionTypes.GET_SURVEY_BY_ID_REQUESTED,
  surveyId,
});

export const setActiveBlock = (blockId) => ({
  type: actionTypes.SET_ACTIVE_BLOCK,
  blockId,
});

export const toggleStateOfBlock = (payload) => ({
  type: actionTypes.TOGGLE_STATE_OF_BLOCK,
  payload,
});

export const changeTitle = (payload) => ({
  type: actionTypes.CHANGE_TITLE,
  payload,
});

export const changeAnswer = (payload) => ({
  type: actionTypes.CHANGE_ANSWER,
  payload,
});

export const createSurvey = (payload) => ({
  type: actionTypes.CREATE_SURVEY_REQUESTED,
  payload,
});

export const saveSurvey = (payload) => ({
  type: actionTypes.SAVE_SURVEY,
  payload,
});

export const changeSurveyTitle = (payload) => ({
  type: actionTypes.CHANGE_SURVEY_TITLE,
  payload,
});

export const resetCreatedState = (payload) => ({
  type: actionTypes.RESET_CREATED_STATE,
  payload,
});



