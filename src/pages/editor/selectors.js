import { createSelector } from 'reselect';

const getQuestions = state => state.editor.questions;
const activeBlock = state => state.editor.activeBlock;

const getQuestionsByBlock = createSelector(
  getQuestions,
  activeBlock,
  (questions, activeBlock) => (
    questions && questions.filter(question => question.block_id === activeBlock)
  )
);

export default getQuestionsByBlock;
