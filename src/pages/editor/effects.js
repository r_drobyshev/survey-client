import {
  all, put, call, fork, takeLatest, select,
} from 'redux-saga/effects';
import axios from 'axios';

import { actionTypes } from './reducer';

function* getSurveyById(action) {
  try {
    const state = yield select();
    axios.defaults.headers.common.Authorization = state.login.token || '';
    const response = yield call(axios, `/api/questions/admin/${action.surveyId}`, {
      method: 'GET',
    });
    yield put({ type: actionTypes.GET_SURVEY_BY_ID_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
  }
}

function* createSurvey(action) {
  try {
    const { questions, blocks, survey } = action.payload;
    const checkedBlocks = [];
    Object.keys(blocks).forEach((key) => {
      if (blocks[key]) checkedBlocks.push(key)
    });
    const questionsToCreate = [];
    questions.forEach(question => {
      checkedBlocks.forEach(block => {
        if (question.block_id === block.split('_')[1]) questionsToCreate.push(question);
      })
    });
    yield call(axios, '/api/survey', {
      method: 'POST',
      data: { questions: questionsToCreate, survey },
    });
    yield put({type: actionTypes.CREATE_SURVEY_SUCCEEDED});
  } catch (e) {
    console.log('-->e', e);
  }
}

function* saveSurvey(action) {
  try {
    yield call(axios, `/api/survey/${action.payload.survey.survey_id}`, {
      method: 'PUT',
      data: action.payload,
    });
    yield put({type: actionTypes.SAVE_SURVEY_SUCCEEDED});
  } catch (e) {
    console.log('-->e', e);
  }
}

function* takeGetSurveyById() {
  yield takeLatest(actionTypes.GET_SURVEY_BY_ID_REQUESTED, getSurveyById);
}

function* takeCreateSurvey() {
  yield takeLatest(actionTypes.CREATE_SURVEY_REQUESTED, createSurvey);
}

function* takeSaveSurvey() {
  yield takeLatest(actionTypes.SAVE_SURVEY, saveSurvey);
}

export default function* () {
  yield all([
    fork(takeGetSurveyById),
    fork(takeCreateSurvey),
    fork(takeSaveSurvey),
  ]);
}
