import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import './Questions.scss';
import { saveResults } from '../../actions';
import Question from './components/Question/Question';

function scrollToTop(scrollDuration) {
  let scrollStep = -window.scrollY / (scrollDuration / 5),
    scrollInterval = setInterval(function(){
      if (window.scrollY !== 0) {
        window.scrollBy( 0, scrollStep );
      }
      else clearInterval(scrollInterval);
    }, 5);
}

class Questions extends React.PureComponent {
  state = {
  };

  componentDidUpdate() {
    scrollToTop(300);
  }

  render() {
    const { props } = this;
    return (
      <>
        <div className="question-list">
          {
            props.questionsByBlock.map((question, questionIndex) => (
              <Question key={question.question_id} question={question} questionIndex={questionIndex} />
            ))
          }
        </div>
      </>
    )
  }
}

const mapDispatchToProps = {
  saveResults,
};

export default withRouter(connect(null, mapDispatchToProps)(Questions));
