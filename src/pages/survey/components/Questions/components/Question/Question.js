import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Favorite from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';

import './Question.scss';
import { setAnswer } from '../../../../actions';

class Question extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state={
      value: '',
      title: props.question.text,
    };
  }

  componentDidUpdate() {
    const { props } = this;
    localStorage.setItem('answers', JSON.stringify(props.answers));
  }

  handleChange = name => event => {
    const { props } = this;
    const answerValue = props.answers[props.question.question_id] ? [...props.answers[props.question.question_id]] : [];
    answerValue[+name] = event.target.checked;
    props.setAnswer({ question_id: event.target.value.split('+')[0], answerValue });
  };

  handleChangeRadio = event => {
    const { props } = this;
    props.setAnswer({ question_id: event.target.name, answerValue: event.target.value });
  };

  handleChangeText = (event) => {
    const { props } = this;
    props.setAnswer({ question_id: event.target.id, answerValue: event.target.value });
  };

  handleChangeNumber = (event) => {
    const { props } = this;
    const regex = /^\d+$/;
    if (!regex.test(event.target.value) && event.target.value !== '') return;
    props.setAnswer({ question_id: event.target.id, answerValue: event.target.value });
  };

  handleChangeSelect = (event) => {
    const { props } = this;
    props.setAnswer({ question_id: event.target.name, answerValue: event.target.value });
  };

  render() {
    const { props, props: { classes }, props: { question: { question_id } } } = this;
    console.log('-->props', props.question);
    return (
      <Card classes={{ root: classes.answerCard }} className="question-card" id={props.question.question_id}>
        <CardContent>
          <h4 className='question-card__title'><b>{`${props.questionIndex+1}. `}</b>{props.question.text}<span>*</span></h4>
          {
            props.question.type === 'multi' && (
              <FormGroup id={props.question.question_id}>
                {
                  JSON.parse(props.question.answer_variants).map((answer, answerIndex) => {
                    return (
                      <FormControlLabel
                        key={answer}
                        classes={{label: classes.labelFont}}
                        className='question-card__answer'
                        control={
                          <Checkbox
                            defaultChecked={props.answers[question_id] ? props.answers[question_id].find((item, index) => index === answerIndex + 1) : false}
                            onChange={this.handleChange(`${answerIndex + 1}`)}
                            value={`${props.question.question_id}+${answerIndex + 1}`}
                          />
                        }
                        label={answer}
                      />
                    )
                  })
                }
              </FormGroup>
            )
          }
          {
            props.question.type === 'single' && (
              <RadioGroup
                name={props.question.question_id}
                value={props.answers[props.question.question_id] ? `${props.answers[props.question.question_id]}` : ''}
              >
                {
                  JSON.parse(props.question.answer_variants).map((answer, answerIndex) => (
                    <FormControlLabel
                      className='question-card__answer'
                      key={`radio1+${answerIndex + 1}`}
                      value={`${answerIndex + 1}`}
                      control={<Radio />}
                      label={answer}
                      onChange={this.handleChangeRadio}
                    />
                  ))
                }
              </RadioGroup>
            )
          }
          {
            props.question.type === 'grade' ? (
              <>
                {
                  props.question.block_id !== '9' ? (
                    <RadioGroup
                      className="grade-radio-group"
                      name={props.question.question_id}
                      value={props.answers[props.question.question_id] ? `${props.answers[props.question.question_id]}` : ''}
                    >
                      {
                        JSON.parse(props.question.answer_variants).map((answer, answerIndex) => (
                          <FormControlLabel
                            className='question-card__answer'
                            key={`radio1+${answerIndex + 1}`}
                            value={`${answerIndex + 1}`}
                            control={<Radio
                              icon={<FavoriteBorder fontSize="large" />}
                              checkedIcon={<Favorite fontSize="large" />}
                            />}
                            label={answer}
                            onChange={this.handleChangeRadio}
                          />
                        ))
                      }
                    </RadioGroup>
                  ) : (
                    <RadioGroup
                      className="grade-radio-group"
                      name={props.question.question_id}
                      value={props.answers[props.question.question_id] ? `${props.answers[props.question.question_id]}` : ''}
                    >
                      {
                        JSON.parse(props.question.answer_variants).map((answer, answerIndex) => (
                          <FormControlLabel
                            className='question-card__answer'
                            key={`radio1+${answerIndex}`}
                            value={`${answerIndex}`}
                            control={<Radio
                              icon={<FavoriteBorder fontSize="large" />}
                              checkedIcon={<Favorite fontSize="large" />}
                            />}
                            label={answer}
                            onChange={this.handleChangeRadio}
                          />
                        ))
                      }
                    </RadioGroup>
                  )
                }
              </>
            ) : null
          }
          {
            props.question.type === 'text' && (
              <TextField
                id={props.question.question_id}
                value={props.answers[props.question.question_id] || ''}
                label=""
                type="text"
                margin="normal"
                onChange={this.handleChangeText}
                placeholder="Введите ответ в поле"
                fullWidth
              />
            )
          }
          {
            props.question.type === 'number' && (
              <TextField
                id={props.question.question_id}
                value={props.answers[props.question.question_id] || ''}
                label=""
                type="number"
                margin="normal"
                onChange={this.handleChangeNumber}
                placeholder="Введите ответ в поле"
                fullWidth
              />
            )
          }
          {
            props.question.type === 'select' && (
              <FormControl className="question-card__answer">
                <InputLabel htmlFor="speciality">{props.answers[props.question.question_id]}</InputLabel>
                <Select
                  value={props.answers[props.question.question_id] || ''}
                  onChange={this.handleChangeSelect}
                  inputProps={{
                    name: props.question.question_id,
                    id: 'speciality',
                  }}
                  className='question-card__answer-select'
                  classes={{ selectMenu: classes.menu }}
                >
                  {
                    JSON.parse(props.question.answer_variants).map((answer) => (
                      answer.map((innerAnswer, index) => {
                        if (index === 0) return (
                          <MenuItem classes={{ root: classes.menuItem }} disabled key={innerAnswer} value={innerAnswer}>{innerAnswer}</MenuItem>
                        );
                        return (
                          <MenuItem classes={{ root: classes.menuItem }} key={innerAnswer} value={innerAnswer}>{innerAnswer}</MenuItem>
                        )})
                    ))
                  }
                </Select>
              </FormControl>
            )
          }
        </CardContent>
      </Card>
    )
  }
}

const styles = () => ({
  labelFont: {
    fontFamily: 'Ubuntu, sans-serif',
  },
  answerCard: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  menu: {
    // whiteSpace: 'inherit',
  },
  menuItem: {
    whiteSpace: 'inherit',
    height: 'auto',
  },
});

const mapStateToProps = state => ({
  answers: state.survey.answers,
});

const mapDispatchToProps = {
  setAnswer,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Question));
