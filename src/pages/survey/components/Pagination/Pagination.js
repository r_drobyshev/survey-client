import React from 'react';
import { connect } from 'react-redux';

import './Pagination.scss';

import { setPageNumOfCurrentPage, increasePageNum, decreasePageNum, setActiveBlockId } from '../../actions';

function isValid(questions, answers, blockId) {
  const questionsByBlock = questions.filter(question => question.block_id === blockId.toString());
  const results = [];
  Object.entries(answers).forEach(([index, answer]) => {
    questionsByBlock.forEach(question => {
      if (question.type === 'multi') {
        if (question.question_id === index && answer.some(item => item)) results.push(true)
      } else {
        if (question.question_id === index && answer) results.push(true)
      }
    })
  });
  return results.length === questionsByBlock.length;
}

class Pagination extends React.PureComponent {
  componentDidMount() {
    const { props } = this;
    props.setActiveBlockId(+props.pages[0]);
  }

  increasePageNumber = () => {
    const { props } = this;
    if (!isValid(props.questions, props.answers, props.blockId)) {
      return alert('Пожалуйста, заполните все обязательные поля!');
    }
    if (props.pages.indexOf(props.blockId.toString()) !== -1 && props.pages.indexOf(props.blockId.toString()) < props.pages.length - 1) {
      props.increasePageNum();
    }
  };

  setPageNumber = (index) => {
    const { props } = this;
    if (!isValid(props.questions, props.answers, props.blockId)) {
      return alert('Пожалуйста, заполните все обязательные поля!');
    }
    props.setPageNumOfCurrentPage(index)
  };

  decreasePageNumber = () => {
    const { props } = this;
    if (props.pages.indexOf(props.blockId.toString()) !== -1 && props.pages.indexOf(props.blockId.toString()) > 0) {
      props.decreasePageNum();
    }
  };

  render() {
    const { props } = this;
    if (props.pages.length < 2) return null;
    return (
      <div className="pagination">
        <div className="pagination_button previous" onClick={this.decreasePageNumber}>
          <i className="fas fa-chevron-left" />
        </div>
        {
          props.pages.map((page, index) => (
            <div key={page} className={`pagination_button ${props.blockId.toString() === page ? 'active' : ''}`} onClick={() => this.setPageNumber(index)}>
              {index+1}
            </div>
          ))
        }
        <div className="pagination_button next" onClick={this.increasePageNumber}>
          <i className="fas fa-chevron-right" />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  blockId: state.survey.blockId,
  questions: state.survey.questions,
  answers: state.survey.answers,
});

const mapDispatchToProps = {
  setPageNumOfCurrentPage,
  increasePageNum,
  decreasePageNum,
  setActiveBlockId,
};

export default connect(mapStateToProps, mapDispatchToProps)(Pagination);
