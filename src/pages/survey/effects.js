import {
  all, put, call, fork, takeLatest,
} from 'redux-saga/effects';
import axios from 'axios';

import { actionTypes } from './reducer';

function* getSurvey(action) {
  try {
    const response = yield call(axios, `/api/questions/survey/${action.surveyId}`, {
      method: 'GET',
    });
    yield put({ type: actionTypes.GET_SURVEY_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
  }
}

function* checkPassword(action) {
  try {
    const response = yield call(axios, `/api/password/${action.payload.id}`, {
      method: 'POST',
      data: { password: action.payload.password },
    });
    yield put({ type: actionTypes.CHECK_PASSWORD_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
    alert('Пароль неверный');
  }
}

function* saveResults(action) {
  const { questions, survey, answers } = action.payload;
  const question_ids = questions
    .filter(question => question.survey_id === survey.survey_id)
    .map(question => question.question_id);
  const entries = Object.entries(answers)
    .map(entry => ({ question_id: entry[0], answer: entry[1] }));
  const results = entries
    .filter((entry) => question_ids.find(id => id === entry.question_id))
    .map(result => {
      return {
        survey_id: survey.survey_id,
        question_id: result.question_id,
        answer: JSON.stringify(result.answer),
      }
    });
  try {
    const response = yield call(axios, `/api/results/${survey.survey_id}`, {
      method: 'POST',
      data: { results, survey },
    });
    localStorage.removeItem('token');
    localStorage.removeItem('answers');
    yield put({ type: actionTypes.SAVE_RESULTS_SUCCEEDED, payload: response.data });
  } catch (e) {
    console.log('-->e', e);
    alert('Произошла ошибка!');
  }
}

function* takeGetSurvey() {
  yield takeLatest(actionTypes.GET_SURVEY_REQUESTED, getSurvey);
}

function* takeCheckPassword() {
  yield takeLatest(actionTypes.CHECK_PASSWORD_REQUESTED, checkPassword);
}

function* takeSaveResults() {
  yield takeLatest(actionTypes.SAVE_RESULTS_REQUESTED, saveResults);
}

export default function* () {
  yield all([
    fork(takeGetSurvey),
    fork(takeCheckPassword),
    fork(takeSaveResults),
  ]);
}
