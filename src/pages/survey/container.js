import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import './survey.scss';
import SurveyMain from './SurveyPageMain';
import SurveyHeader from '../../components/SurveyHeader/SurveyHeader';
import { getSurvey } from './actions';

class SurveyPage extends React.PureComponent {
  componentDidMount() {
    const { props, props: { match: { params: { id } } } } = this;
    props.getSurvey(id);
  }

  render() {
    const { props } = this;
    if (!props.questions) return null;
    if (props.survey.name) document.title = props.survey.name;
    return (
      <>
        <SurveyHeader title={props.survey.name} />
        <SurveyMain />
      </>
    )
  }
}

const mapStateToProps = state => ({
  questions: state.survey.questions,
  survey: state.survey.survey,
  answers: state.survey.answers,
});

const mapDispatchToProps = {
  getSurvey,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SurveyPage));