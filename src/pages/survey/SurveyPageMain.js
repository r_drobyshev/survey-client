import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router';

import './survey.scss';
import Questions from './components/Questions/Questions';
import Pagination from './components/Pagination/Pagination';
import { checkPassword, saveResults } from './actions';
import getQuestionsByBlock from './selectors';
import  { getBlockDescription } from '../../helper/utils';

function isValid(questions, answers) {
  const results = [];
  Object.entries(answers).forEach(([index, answer]) => {
    questions.forEach(question => {
      if (question.type === 'multi') {
        if (question.question_id === index && answer.some(item => item)) results.push(true)
      } else {
        if (question.question_id === index && answer) results.push(true)
      }
    })
  });
  return results.length === questions.length;
}

function Disabled() {
  return (
    <div className="body-wrapper survey-main">
      <Card className="survey-disabled-window">
        <CardContent>
          <h3>Опрос закрыт!</h3>
        </CardContent>
      </Card>
    </div>
  )
}


class SurveyPageMain extends React.PureComponent {
  state = {
    password: '',
    finish: false,
  };

  handleChangePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  saveResults = () => {
    const { props, props: { questions, answers, survey } } = this;
    if (!isValid(props.questions, props.answers, props.blockId)) {
      return alert('Пожалуйста, заполните все обязательные поля!');
    }
    props.saveResults({ questions, answers, survey });
  };

  render() {
    const { props, state: { password }, props: { match: { params: { id } } }, props: { blockId } } = this;
    if (!props.survey.state) return <Disabled />;
    return (
      <div className="body-wrapper survey-main">
        {
          props.isTokenExist ? (
            <>
              <Card className="block-desc-card"><CardContent>{getBlockDescription(blockId)}</CardContent></Card>
              <Questions questionsByBlock={props.questionsByBlock} />
              <div className="send-button-wrapper">
                <Button
                  disabled={!isValid(props.questions, props.answers)}
                  onClick={this.saveResults}
                  className={`survey-send-button ${blockId.toString() === props.pages[props.pages.length - 1] ? 'show' : ''}`}
                  variant="contained"
                >
                  Отправить
                </Button>
                <Button
                  disabled={!isValid(props.questions, props.answers)}
                  onClick={this.saveResults}
                  className={`survey-send-button mobile ${blockId.toString() === props.pages[props.pages.length - 1] ? 'show' : ''}`}
                  variant="contained"
                >
                  <i className="far fa-paper-plane" />
                </Button>
              </div>
              <Pagination pages={props.pages} />
            </>
          ) : props.finished ? (
            <Card className="thank-you-card">
              <CardContent className="ty-content">
                <i className="far fa-check-circle" />
                <h3>Благодарим за заполнение анкеты!</h3>
              </CardContent>
            </Card>
            ) : (
            <Card className="check-password-card">
              <CardContent>
                <p>
                  Уважаемый респондент! Мы просим Вас принять участие в исследовании отношения
                  студентов к обучению. Отвечайте, пожалуйста, искренне. Нас интересуют только
                  обобщенные данные. Мы гарантируем Вам неразглашение индивидуальных ответов.
                  Благодарим Вас за сотрудничество!
                </p>
                <p>Для доступа к опросу, пожалуйста, введите пароль!</p>
                <TextField
                  id="standard-password-input"
                  value={password}
                  label="Password"
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                  onChange={this.handleChangePassword}
                  fullWidth
                />
                <Button className="survey-send-password-button" onClick={() => props.checkPassword({id, password})} variant="contained">Начать опрос</Button>
              </CardContent>
            </Card>
          )
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  questions: state.survey.questions,
  answers: state.survey.answers,
  survey: state.survey.survey,
  pages: state.survey.blocks,
  questionsByBlock: getQuestionsByBlock(state),
  isTokenExist: state.survey.isTokenExist,
  blockId: state.survey.blockId,
  finished: state.survey.finished,
});

const mapDispatchToProps = {
  checkPassword,
  saveResults,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SurveyPageMain));
