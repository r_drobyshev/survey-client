import immutable from 'seamless-immutable';

import { compareBlocks } from '../../helper/utils';
// import answers from './answers';

export const actionTypes = {
  GET_SURVEY_REQUESTED: 'survey/GET_SURVEY_REQUESTED',
  GET_SURVEY_SUCCEEDED: 'survey/GET_SURVEY_SUCCEEDED',
  CHECK_PASSWORD_REQUESTED: 'survey/CHECK_PASSWORD_REQUESTED',
  CHECK_PASSWORD_SUCCEEDED: 'survey/CHECK_PASSWORD_SUCCEEDED',
  SET_PAGE_NUMBER_OF_CURRENT_PAGE: 'survey/SET_PAGE_NUMBER_OF_CURRENT_PAGE',
  INCREASE_PAGE_NUMBER: 'survey/INCREASE_PAGE_NUMBER',
  DECREASE_PAGE_NUMBER: 'survey/DECREASE_PAGE_NUMBER',
  SET_ACTIVE_BLOCK_ID: 'survey/SET_ACTIVE_BLOCK_ID',
  SET_ANSWER: 'survey/SET_ANSWER',
  SAVE_RESULTS_REQUESTED: 'survey/SAVE_RESULTS_REQUESTED',
  SAVE_RESULTS_SUCCEEDED: 'survey/SAVE_RESULTS_SUCCEEDED',
};

function checkToken() {
  return localStorage.getItem("token");
}

const initialState = immutable({
  isTokenExist: Boolean(checkToken()),
  // isTokenExist: true,
  blockId: 1,
  answers: JSON.parse(localStorage.getItem('answers')) || {},
  finished: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_SURVEY_REQUESTED:
      return {
        ...state,
      };
    case actionTypes.GET_SURVEY_SUCCEEDED:
      return {
        ...state,
        survey: action.payload.survey,
        questions: action.payload.questions,
        blocks: action.payload.questions.map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index)),
      };
    case actionTypes.CHECK_PASSWORD_SUCCEEDED:
      localStorage.setItem("token", action.payload);
      return {
        ...state,
        isTokenExist: true,
      };
    case actionTypes.SET_PAGE_NUMBER_OF_CURRENT_PAGE:
      return {
        ...state,
        blockId: +state.blocks[action.pageNum],
      };
    case actionTypes.INCREASE_PAGE_NUMBER:
      return {
        ...state,
        blockId: +state.blocks[state.blocks.indexOf(state.blockId.toString()) + 1],
      };
    case actionTypes.DECREASE_PAGE_NUMBER:
      return {
        ...state,
        blockId: +state.blocks[state.blocks.indexOf(state.blockId.toString()) - 1],
      };
    case actionTypes.SET_ACTIVE_BLOCK_ID:
      return {
        ...state,
        blockId: action.blockId,
      };
    case actionTypes.SET_ANSWER:
      const { question_id } = action.payload;
      return {
        ...state,
        answers: { ...state.answers, [question_id]: action.payload.answerValue },
      };
    case actionTypes.SAVE_RESULTS_SUCCEEDED:
      return {
        ...state,
        finished: true,
        isTokenExist: false,
      };
    default:
      return state;
  }
}