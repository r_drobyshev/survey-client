import { actionTypes } from './reducer';

export const getSurvey = (surveyId) => ({
  type: actionTypes.GET_SURVEY_REQUESTED,
  surveyId,
});

export const checkPassword = (payload) => ({
  type: actionTypes.CHECK_PASSWORD_REQUESTED,
  payload,
});

export const setPageNumOfCurrentPage = pageNum => ({
  type: actionTypes.SET_PAGE_NUMBER_OF_CURRENT_PAGE,
  pageNum,
});

export const increasePageNum = pageNum => ({
  type: actionTypes.INCREASE_PAGE_NUMBER,
  pageNum,
});

export const decreasePageNum = pageNum => ({
  type: actionTypes.DECREASE_PAGE_NUMBER,
  pageNum,
});

export const setActiveBlockId = blockId => ({
  type: actionTypes.SET_ACTIVE_BLOCK_ID,
  blockId,
});

export const setAnswer = payload => ({
  type: actionTypes.SET_ANSWER,
  payload,
});

export const saveResults = payload => ({
  type: actionTypes.SAVE_RESULTS_REQUESTED,
  payload,
});

