import { createSelector } from 'reselect';

const getQuestions = state => state.survey.questions;
const blockId = state => state.survey.blockId;

const getQuestionsByBlock = createSelector(
  getQuestions,
  blockId,
  (questions, blockId) => (questions.filter(question => question.block_id === blockId.toString()))
);

export default getQuestionsByBlock;
